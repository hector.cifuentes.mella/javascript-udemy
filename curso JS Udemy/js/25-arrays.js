'use strict';

//Arrays, Arreglos, Matrices

var nombre = "Hector Cifuentes";

var nombres = ["Hector cifuentes","Juan Lopez","Manuel Garcia", "Steve Jobs"];

var lenguajes = new Array("PHP" , "JS" , "GO" , "JAVA");

// console.log(nombres);
// console.log(lenguajes);
// console.log(nombres[2]);
// console.log(lenguajes[2]);

//Con length podemos ver el largo del arreglo
console.log(nombres.length);

// var elemento = parseInt(prompt("¿Que elemento del array quieres?"));
// if (elemento >= nombres.length){
//     alert("Introduce el numero correcto, tiene que ser menor que: "+nombres.length);
// } else {
//     alert("El usuario seleccionado es : "+nombres[elemento]);
// }
// document.write(
//     ` <h1>Lenguajes de programacion del 2018 </h1>
// `);
// for (var i=0 ; i<lenguajes.length;i++){
//     document.write("<li>"+ lenguajes[i]+"</li>");
// }

//El foreach entrega el arreglo completo utilizando los callbacks
// lenguajes.forEach((elemento,index,arr)=>{
//     document.write("<li>"+index+" "+elemento+"</li>");
//     //arr tiene el arreglo completo
//     console.log(arr);
// });
//document.write("<ul>");
for (let lenguaje in lenguajes){
    document.write("<li>"+lenguajes[lenguaje]+"</li>");
}

//Clase busquedas

var busqueda = lenguajes.find(lenguaje => lenguaje === "PHP");
var busqueda1 = lenguajes.findIndex(lenguaje => lenguaje === "PHP");
console.log(busqueda);
console.log(busqueda1);