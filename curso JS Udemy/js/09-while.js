'use strict'

//Bucle While

var year =2018;

while (year !== 1991){
    //Ejecuta el bloque
    console.log("Estamos en el año: "+ year);
    if(year === 2000){
        break;
    }
    year--;
}

//DO While

var years = 30;
do{
    alert(" Solo cuando sea diferente a 20");
    years--;
}while (years > 25);
