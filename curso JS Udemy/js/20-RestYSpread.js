'use strict';

//Parametros Rest y Spread
//con los ... se puede capturar todos los datos del arreglo
function listadoFrutas(fruta1,fruta2, ...resto_de_frutas) {
    console.log("fruta 1 : "+fruta1);
    console.log("fruta 2 : "+fruta2);
    console.log(resto_de_frutas);
}

listadoFrutas("naranja","Manzana","Sandia","Pera","Melon","Coco");

//Spread
var frutas = ["Naranja","Manzana"];
listadoFrutas(...frutas,"Sandia","Pera","Melon","Coco");
