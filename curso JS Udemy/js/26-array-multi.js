'use strict';

var categorias = ['Accion','Terror','Comedia'];

var peliculas = ['La verdad duele', 'la vida es bella','Gran torino'];
var cine = [categorias,peliculas];

//Sort permite ordenar alfabeticamente
peliculas.sort();
//Darle la vuelta al array
peliculas.reverse();
// console.log(cine[0][1]);
// console.log(cine[1][2]);
/*
do {
    var elemento = prompt("Introduce tu peliculas");
    //if (elemento !== "ACABAR") {
    peliculas.push(elemento);
    //}
}while (elemento !== "ACABAR");
peliculas.pop();
*/
var indice = peliculas.indexOf('Gran torino');
if (indice> -1){
    peliculas.splice(indice,1);
}
//Convertir un array a string separado por coma
var datos = peliculas.join();
console.log(datos);

var cadena = "texto1, texto2, texto3";
var cadena_array = cadena.split(", ");
// console.log(indice);
// console.log(peliculas);
