// Variables
//Variables son contenedores de información
// Con var se pueden modificar las variables
//Con let se usa en typescript y js con el ecma 6 no permite modificar las variables
'use strict';
var pais = "Chile";
var continente = 'Europa';
var antiguedad =2019;
var pais_y_continente = pais+ ' '+continente;

pais = "Mexico";
continente= "Latinoamerica";
console.log(pais,continente,antiguedad);
alert(pais_y_continente);