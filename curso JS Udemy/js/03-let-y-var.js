'use strict'

//Pruebas con let y var

//Pruebas con var
var numero=40;
console.log(numero);

if (true){
    var numero = 50;
    console.log(numero);
}
console.log(numero);

//Pruebas con let

var texto= "Curso de JS";
console.log(texto);

if (true){
    let texto= "Curso de Laravel ";
    console.log(texto); //Imprimira lo que esta dentro del bloque if
}
console.log(texto);//Imprime el valor anterior