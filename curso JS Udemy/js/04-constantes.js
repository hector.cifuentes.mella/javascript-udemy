'use strict';

//Constantes
//Como una variable, pero su valor no puede cambiar

var web = "https://google.com";
//No puedo asignar otro valor a la constante
const ip= "192.168.1.1";

web= "https://google.cl";

console.log(web,ip);
