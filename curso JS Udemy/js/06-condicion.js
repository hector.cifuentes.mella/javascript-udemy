'use strict'

//Condiconal IF

var edad = 18;
var nombre = 'David Suarez';

if  (edad   >=  18){
    console.log(nombre+ " tiene "+edad+"años");

    if  (edad   ==  33){
        console.log('Todavia eres millenial');
    }
}
else{
    console.log(nombre+ " tiene "+edad+"años, es menor de edad");
}

/*
* Operadores Logicos
* AND(y): &&
* OR(O): ||
* Negación: !
* */

//Negación
var year = 2018;
if (year !== 2016){
    console.log("El año no es 2016 es : " + year);
}

//AND
if (year >= 2000 && year <=2020){
    console.log("Estamos en la era actual");
}else{
    console.log("Estamos en la era postmoderna");
}

//OR
if (year == 2018 || year >= 2018 && year == 2028){
    console.log("El año acaba en 8");
}else {
    console.log("año no registrado");
}