'use strict';

//Eventos del raton

//var boton = document.querySelector("#boton");

function cambiaColor() {
    console.log("Me has dado click");
    var bg = boton.style.background;
    if  (bg ==="green"){
        boton.style.background = "red";

    }
    else{
        boton.style.background = "green";
    }
    boton.style.padding = "10px";
    boton.style.border = "1px solid #ccc";
}
var boton = document.querySelector("#boton");

//Con addEventListener captura cualquier evento
boton.addEventListener('click', () =>{
    cambiaColor();
});

//Mouse over cuando pasas por encima de algo lanza el evento
boton.addEventListener('mouseover', ()=>{
   boton.style.background = "#ccc";
});

//Mouse out
boton.addEventListener('mouseout', ()=>{
   boton.style.background= "black";
});

//Focus cuando se hace foco en un input ejecuta una funcion
var input = document.querySelector("#campo_nombre");
input.addEventListener('focus', ()=>{
    console.log("Estas dentro del input");
});

//Blur lo mismo que focus pero al salir del evento
input.addEventListener('blur', ()=>{
    console.log("Estas fuera del input");
});

//Keydown se activa al escribir cualquier tecla
input.addEventListener('keydown', (event) => {
    console.log("Estas pulsando esta tecla en el input", String.fromCharCode(event.keyCode));
});

//Keypress es la tecla presionada por el usuario
input.addEventListener('keypress', (event) =>{
    console.log("Tecla presionada : " , String.fromCharCode(event.keyCode))
});

//Keyup
input.addEventListener('keyup',(event)=>{
    console.log("Tecla Soltada", String.fromCharCode(event.keyCode));
});