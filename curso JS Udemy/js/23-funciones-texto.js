'use strict';

//Transformación de textos

var numero = 44;
var texto1 = "Bienvenidos al curso de curso de Javascript";
var texto2 = "Es buen curso";

//Busqueda de en que posicion aparece la palabra
var busqueda = texto1.indexOf("curso");
//Busqueda de la ultima aparicion de la palabra
var busqueda2 = texto1.lastIndexOf("curso");
//Funciona similar a search
var busqueda3 = texto1.search("curso");
//Devuelve un array con todas las concordancias
var busqueda4 = texto1.match(/curso/g);
//Sacar desde , las siguientes n letras
var busqueda5 = texto1.substr(15,5);
//obtiene una letra utilizando el indice
var busqueda6 = texto1.charAt(24);
//Busco un texto si lo encuentra dice true o false en el inicio del string
var busqueda7 = texto1.startsWith("Bie");
//Busqueda en el final del texto true o false siesque lo encuentra
var busqueda8 = texto1.endsWith("Javascript");
//Busca el string una palabra dice true o false se puede buscar en posiciones
var busqueda9 = texto1.includes("curso");
// console.log(busqueda);
// console.log(busqueda2);
// console.log(busqueda3);
//console.log(busqueda4);
//console.log(busqueda5);
//console.log(busqueda6);
//console.log(busqueda7);
//console.log(busqueda8);
//console.log(busqueda9);
/*
*
* Clase funciones de Reemplazo
*
*/
//Cambia el texto por otro
var busqueda10 = texto1.replace("Javascript","javascript");
//console.log(busqueda10);

//Devuelve un string a partir del indice entregado en slice se puede cortar del inicio y el final
var busqueda11 = texto1.slice(15);
//console.log(busqueda11);

//El split separa en un arreglo todos las palabras despues de cada espacio vacio
var busqueda12 = texto1.split(" ");
//console.log(busqueda12);

//Quita los espacios vacios en el inicio y el final
var busqueda13 = texto1.trim();
//console.log(busqueda13);



/*

//Convierte numero en string o cualquier cosa

var dato = numero.toString();
dato = texto1.toUpperCase();
dato = texto2.toLowerCase();
//console.log(dato);

//Calcular Longitud

var nombre = "";
//console.log(nombre.length);

//Concatenar Textos

// var textoTotal = texto1+" "+texto2;

var textoTotal= texto1.concat(" "+texto2);
console.log(textoTotal);*/
