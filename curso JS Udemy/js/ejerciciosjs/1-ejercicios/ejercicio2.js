'use strict';
/*
*Utilizando un bucle, mostrar la suma y la media de los numeros introducidos
* hasta ingresar un numero negativo y ahi mostrar el resultado
*/

var indice = 0;
var sumaacumulada = 0;

do {
    var numero = parseInt(prompt("Ingrese un numero"));
    if (isNaN(numero)){
        numero = 0;
    }else if ( numero>=0) {
        sumaacumulada = sumaacumulada + numero;
        indice++;
    }
}while(numero >= 0);

alert("La suma es de :" + sumaacumulada);
alert("La media es de :" +(sumaacumulada/indice));