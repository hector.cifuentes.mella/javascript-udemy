'use strict';
/*
* Pida 6 numeros por pantalla y guardelos en un array
* Mostrar el array entero en el cuerpo del programa y la consola
* Ordenarlo y mostrarlo
* Mostrar cuantos elementos tiene el array
* Busqueda de un valor introducido por el usuario, que nos diga si lo encontro y su indice
* Se valora el uso de funciones
*/
function mostrarArrays(numbers,textoCustom = "") {
    document.write("<h1>"+ "Arreglo: "+textoCustom+"</h1>")
    numbers.forEach((elemento,index) =>{
        document.write("<li>"+"En la posicion: "+index+" - El elemento del arreglo es: "+elemento+"</li>");
        //console.log(elemento);
    });
    document.write("</br>");
}
var numeros = [];
do {
    let numero = parseInt(prompt("Ingrese un numero"));
    if (isNaN(numero) !== true){
        numeros.push(numero);
        //console.log(numeros);
        var largo = numeros.length;
        //console.log(largo);
    }
}while(largo<6);

//Largo del arreglo
document.write("El largo del arreglo es de: "+largo+"</br>");
document.write("</br>");


mostrarArrays(numeros);
//Busqueda
var buscador = parseInt(prompt("Ingrese el valor a buscar"));
console.log(buscador);
var busqueda = numeros.find( numero => numero === buscador);
console.log(busqueda);
if (busqueda !== -1) {
    var busqueda1 = numeros.findIndex( numero => numero === buscador);
    document.write("El elemento: "+buscador+" Se encuentra en la posicion : "+busqueda1+"</br>");
}else{
    alert("El elemento no se encuentra en la lista");
}

//Ordenarlo y mostrarlo
var ordenados = numeros.sort();
mostrarArrays(ordenados, "ordenados");

//Desordenado
var desordenados = numeros.reverse();
mostrarArrays(desordenados,"desordenados");


