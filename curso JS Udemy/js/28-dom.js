'use strict';

//Dom document object model
function cambiaColor(color) {
    caja.style.background = color;
}

//Selecciona basado en el ID
//var caja = document.getElementById("micaja");

//Selecciona por la etiqueta de tipo div
var caja = document.querySelector("#micaja");
//Selecciona el html del div
caja.innerHTML = " !Texto en la caja desde JS";

//Reglas de css
//caja.style.background = "red";
caja.style.padding = "20px";
caja.style.color = "white";
caja.className = "hola";
cambiaColor("green");

//Conseguir elementos por etiqueta
var todosLosDivs = document.getElementsByTagName('div');
//console.log(todosLosDivs);

var seccion = document.querySelector("#miseccion");
var hr = document.createElement("hr");

//Con InnerHTML se pueden asignar elementos al div

//Con for in hay que dejar el valor inicializado
var valor;
for (valor in todosLosDivs) {
    //con textContent sacan el contenido del HTML
    if (typeof(todosLosDivs[valor].textContent) === 'string'){
        var parrafo = document.createElement("p");
        var texto = document.createTextNode(todosLosDivs[valor].textContent);
        parrafo.append(texto);
        seccion.append(parrafo);
    }
}
seccion.append(hr);
// var contenidoEnTexto = todosLosDivs[2];
// contenidoEnTexto.style.background = "red";

// contenidoEnTexto.innerHTML = "Otro valor para el segundo elemento";
// console.log(contenidoEnTexto);
// console.log(todosLosDivs);
//console.log(caja);

//Conseguir elementos por su clase css

//var divRojos = document.getElementsByTagName('rojo');
var divRojos = document.getElementsByClassName('rojo');
var divAmarillos = document.getElementsByClassName('amarillo');
//console.log(divRojos);
for (var div in divRojos){
    //console.log(divRojos[div]);
    if (divRojos.className === "rojo"){
        divRojos[div].style.background = "red";
    }

}

//Query Selector
var id = document.querySelector("#encabezado");
//console.log(id);

var claseRojo = document.querySelectorAll("h1");
console.log(claseRojo);

var etiqueta = document.querySelectorAll("div");
//console.log(etiqueta);

//El query Selector selecciona un id en especifico para colecciones es mejor el getElementsByClassName o el ByTag