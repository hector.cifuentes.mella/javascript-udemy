'use strict'

//Operadores

var numero1 = 7;
var numero2 = 12;

var operacion = numero1 * numero2;
alert("El resultado es de " + operacion);

var numero_entero = 44;
var cadena_texto = "Hola que tal ";
console.log(cadena_texto);
console(numero_entero);
var verdadero_o_falso = true;

console.log(verdadero_o_falso);

var numero_falso = "33";

//Convierte  un string en un numero
Number(numero_falso);

//Convierte string a entero
parseInt(numero_falso);

//Convierte un numero en string
String(numero_falso);

//Con typeof podemos ver el tipo de dato
typeof (numero_falso);